## Hobby App

This is a simple application that allows a user to register, login and add hobbies. This application sends emails and SMS notifications to users for each hobby they create.

###Technologies that will be used to build this application are

* Front end `ReactJS` and Back end `Sails.js`

* For Email and SMS notification, `Amazon SES` and `Twilo` will be used respectively

* [Link to the Hosting on Heroku](#)

* [Link to the Explainer Video](#)

* [Link to the Postman Collection](#)

* [Link to the Trello Board](https://trello.com/b/a20YWRCl/hobby-app)


###Wire Frame of this application









